# swapsize

## About

This is a simple bash script, which -- if used with crontab, for example -- can monitor the swap size (the size of `/var/vm` folder) and report of changes if the [terminal-notifier](https://github.com/julienXX/terminal-notifier) utility is installed.
